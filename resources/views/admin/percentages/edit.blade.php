@extends('admin.index')
@section('content')


<div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ $title }}</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>aurl('percentages/'.$percentage->id),'method'=>'put' ]) !!}
     <div class="form-group">
        {!! Form::label('percentage','percentage') !!}
        {!! Form::number('percentage',$percentage->percentage,['class'=>'form-control']) !!}
     </div>
     
     <div class="form-group">
        {{$percentage->employee->name}}
        {!! Form::hidden('employee_id',$percentage->employee_id)!!}
        
     </div>

       


     {!! Form::submit(trans('admin.save'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection