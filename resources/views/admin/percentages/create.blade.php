@extends('admin.index')
@section('content')


<div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ $title }}</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>aurl('percentages')]) !!}
     <div class="form-group">
        {!! Form::label('percentage','percentage') !!}
        {!! Form::text('percentage',0.0,['class'=>'form-control']) !!}
     </div>
     
     <div class="form-group">
        {!! Form::label('description',trans('admin.description')) !!}
        {{ Form::select('employee_id', $employess->pluck('name','id'), null, ['class' => 'form-control']) }}


        
     </div>

    
     {!! Form::submit(trans('admin.add'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection